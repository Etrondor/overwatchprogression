$j = require('jquery');
var hasConfig = false;
var gameMode = "quickplay";
var ipc = require('electron').ipcRenderer;
var registerUserButton = document.getElementById('save-users-button');

registerUserButton.addEventListener('click', function(){
    var userCfg = getUsersParms();
    ipc.send('saveUsersNames', userCfg);
    ipc.on('saveUsersNamesReply', function(response,data){
        processResponse(data);
    });
});

// Donne le mode actuel de la recherche
function getCurrentMode() {
	if (typeof gameMode == "undefined") {
		return $j('#modePlay').val()
	}
    return gameMode;
}

function processResponse(rep){
    $j('#register').removeClass("visible");
    $j('#usersAccount').addClass("visible");
    displayProfiles();
}

function showConfig() {
    $j('#register').addClass("visible");
    $j('#usersAccount').removeClass("visible");
}

function getUsersParms(){
    var usersTxt = $j('#users').val();
    usersArray = usersTxt.split('\n');
    for (var i = usersArray.length - 1; i >= 0; i--) {
        if (usersArray[i] != "") {  
            usersArray[i] = usersArray[i].split('#');
            usersArray[i] = usersArray[i].join('-');
        }
    }
    return usersArray;
};




/*
  Début du programme 
  did-finish-load est l'évenement qui anonce le chargment de la page
*/
$j(function(){
    ipc.send('did-finish-load');
    ipc.on('users-list-exist', (event, data) => {
        console.log(data)
        userName = data[0]
        userData = data[1]
        if (data == "false") {
	        $j('#register').addClass("visible");
      	} else {
            displayProfiles();
        }
    });

    $j('#mainMenu select').change(function(){
        $j('#compare').addClass('visible');
    });

    $j('#compare').click(function(){
        var mode = $j('#modePlay').val();
        gameMode = mode;
        var hero = $j('#hero').val();
        var params = {
            "mode" : mode,
            "hero" : hero
        }
        $j('#listUsersAccount').html('');
        ipc.send('new-compare-params', params);
        ipc.send('get-profile-data');
    })
});
