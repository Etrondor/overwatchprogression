// Overwatch node module
var overwatchConnector = function(window) {
	var mainWindow = window
   	var self = this
   	var usersProfiles = []
   	var usersList
   	var modeSearch = "quickplay"
   	var heroSearch = "allHeroes"

   	self.getDataForParams = function(searchParam){
   		modeSearch = searchParam.mode
   		heroSearch = searchParam.hero
   	}

   	self.getAllProfiles = function(){
        // console.log(userList)
        var result = []
        for (var i = 0; i < userList.length; i++) {
            if (userList[i] != "") {
                result[i] = self.getUserInfo(userList[i])
            }
        }
        return result
    }

    // Get all heros profiles
   	self.getAllHeroProfiles = function() {
   		result = []
   		// console.log(userList)
	    for (var i = 0; i < userList.length; i++) {
	      	if (userList[i] != "") {
	        	result[i] = self.getUserHeroStats(userList[i], false)
	      	}
	    }
	    return result
   	}
   
   	self.addUserData = function(userData,ind) {
		// console.log(userActions)
		if (userData != undefined && userData != "") {
			if (typeof ind !== undefined) {
				var listLength = usersProfiles.length
				usersProfiles[listLength] = userData
				userActions.emit('updateUserList')
			} else {
				usersProfiles[ind] = userData
				userActions.emit('updateUserList')
			}
		}
   	}

   	self.getUserInfo = function(userName) {
		var profile
		var request = require("request")
		var EventEmitter = require("events").EventEmitter
		var body = new EventEmitter()
		var requestResp = request('https://api.lootbox.eu/pc/eu/'+userName+'/profile', function (error, response, data) {
			if (!error && response.statusCode == 200) {
				body.data = data
				body.emit('update')
				return body
			}
		})
		body.on('update', function () {
			dataList = [userName, body.data]
			self.sendUserData(dataList)
		})
	}

	self.sendUserData = function(userData) {
		if (modeSearch == "competitive") {
	    	mainWindow.webContents.send('users-data-list-comp',userData);
		} else {
	    	mainWindow.webContents.send('users-data-list-cla',userData);
		}
   	}

   	self.sendMoreInfo = function(result) {
	    mainWindow.webContents.send('get-more-info',result);
   	}

   	self.addUserHeroData = function(userData, ind) {
	    if (userData != undefined && userData != "") {
	      	if (typeof ind !== undefined) {
	        	var listLength = UserHeroStats.length
	        	UserHeroStats[listLength] = userData
	        	userHeroActions.emit('updateUserHeroList')
	      	} else {
	        	UserHeroStats[ind] = userData
	        	userHeroActions.emit('updateUserHeroList')
	      	}
	    }
   	}

   	// Response of : get-user-hero-data
   	self.getUserHeroStats = function(userName) {
   		// https://api.lootbox.eu/pc/eu/Nange-2751/quick-play/allHeroes/
   		// https://api.lootbox.eu/pc/eu/k3rmito-2206/quickplay/allHeroes
	    var profile
	    var request = require("request")
	    var EventEmitter = require("events").EventEmitter
	    var body = new EventEmitter()
	    if (heroSearch == "allHeroes") {
	    	url = 'https://api.lootbox.eu/pc/eu/'+userName+'/'+modeSearch+'/allHeroes/'	
	    } else {
	    	url = 'https://api.lootbox.eu/pc/eu/'+userName+'/'+modeSearch+'/hero/'+heroSearch	
	    }
	    console.log(url)
	    request(url, function (error, response, data) {
	      	if (!error && response.statusCode == 200) {
	        	body.data = data
	        	body.emit('update')
	        	return body
	      	}
	    })
	    body.on('update', function () {
	      	var reusltData = [userName, body.data]
	      	self.sendMoreInfo(reusltData)
	    })
   	}
}
module.exports = overwatchConnector;