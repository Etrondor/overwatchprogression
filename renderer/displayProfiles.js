//Used to display general information
// Load general profile and some games info
function displayProfiles() {
	var nowMode = getCurrentMode();
	if (nowMode == "competitive") {
		displayProfilesCompetitive();
	} else {
		displayProfilesClassic();
	}
}

// ASK FOR Print profile classic Mode
// Respond on : users-data-list-cla
function displayProfilesClassic(){
	$j('#usersAccount').addClass("visible");
	ipc.send('get-profile-data');
}

// ASK FOR Print profile competitive Mode
// Respond on : users-data-list-comp
function displayProfilesCompetitive(){
	$j('#usersAccount').addClass("visible");
	ipc.send('get-profile-data');
}

// Called after general profile loaded
function getMoreContent(userName, mode) {
	console.log(userName+" "+mode)
	data = [mode, userName];
    ipc.send('get-user-hero-data', data);
}

// Response OF NODE SERVER

// When server has load general profile data
// Print those info
ipc.on('users-data-list-cla', (event, data) => {
	$j.get('tpl/profil-classique.html', function(template) {
		var accountName = data[0];
		userData = JSON.parse(data[1]);
		console.log(accountName);
		console.log(userData);
		var isRanked = true;
		if (userData.data.competitive.rank == null) {
			isRanked = false;
		}
	    var rendered = Mustache.render(template, {
	    	"userData": userData,
	    	"accountName" : accountName,
	    	"isRanked" : isRanked
	    });
		$j('#listUsersAccount').append(rendered);
		var mode = getCurrentMode();
		getMoreContent(accountName, mode);
	});
});

// Print games informations
ipc.on('get-more-info', (event, data) => {
    console.log(data)
    userNameReceived = data[0];
    userHero = JSON.parse(data[1]);
	$j.get('tpl/more-data.html', function(template) {
	    var rendered = Mustache.render(template, {"userHero": userHero});
	    $j('#more-info-'+userNameReceived).html(rendered);
	});
});

// When server has load general profile data with competitive param
// Print those info
ipc.on('users-data-list-comp', (event, data) => {
	var accountName = data[0];
	userData = JSON.parse(data[1]);
	$j.get('tpl/profil-competitive.html', function(template) {
		var isRanked = true;
		if (userData.data.competitive.rank == null) {
			isRanked = false;
		}
	    var rendered = Mustache.render(template, {
	    	"userData": userData,
	    	"accountName" : accountName,
	    	"isRanked" : isRanked
	    });
		$j('#listUsersAccount').append(rendered);
	});
	var mode = getCurrentMode();
	getMoreContent(accountName, mode);
});