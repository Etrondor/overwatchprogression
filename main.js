const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
// const {BrowserWindow} = require('electron')


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
    // Create the browser window.
    mainWindow = new BrowserWindow({width: 800, height: 1000})

    const connector = require('./overwatchConnector.js')
    const overwatch = new connector(mainWindow)

    var fs = require('fs');
    var usersPath = app.getPath('userData');
    var userListPath = usersPath+"/users.cfg";

    var EventEmitter = require("events").EventEmitter;
    var userActions = new EventEmitter();
    var usersList = [];
    var usersProfiles = [];

    var userHeroActions = new EventEmitter();
    var UserHeroStats = [];

    var completeDataActions = new EventEmitter();
    var completeData = [];

    // and load the index.html of the app.
    mainWindow.loadURL(`file://${__dirname}/index.html`)
    mainWindow.setTitle("Overwatch progression")
    mainWindow.setMenu(null);
    // Open the DevTools.
    mainWindow.webContents.openDevTools()
    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    });

  
    var ipc = require('electron').ipcMain;

    // On users list change
    ipc.on('saveUsersNames', function(event, data){
        var result = processData(data);
        event.sender.send('saveUsersNamesReply', result);
    });

    // On users list change
    // First listing
    ipc.on('get-profile-data', function(event, data){
        profiles = overwatch.getAllProfiles()
    })

    // On users list change
    ipc.on('get-hero-profile-data', function(event, data){
        overwatch.getAllHeroProfiles();
    });

    // Loading more profile data
    ipc.on('get-user-hero-data', function(event, data){
    	console.log(data)
    	modeSearch = data[0]
    	userName = data[1]
        overwatch.getUserHeroStats(userName)
    });

    // Loading more profile data
    ipc.on('new-compare-params', function(event, data){
        console.log("New search :")
        console.log(data)
        overwatch.getDataForParams(data)
        // overwatch.getUserHeroStats(data)
    });

    //After Loading
    ipc.on('did-finish-load',function(){
        checkUserConfig();
    });

    // Tell to client if config file exist and set userlist in @overwatch object
    function checkUserConfig(){
    	// console.log("check user config");
        usersProfiles = [];
        UserHeroStats = [];
        completeData = [];
        var fileExist;
        fs.exists(userListPath, function(exists){
            if(exists){
                var usersTxtList = fs.readFile(userListPath, 'utf8',function(err,data){
                    if (err){
                        throw err;
                    }
                    userList = data.split("\n"); 
                    if (data == "") {
                        mainWindow.webContents.send('users-list-exist',"false");
                        return false;
                    }else{
                    	overwatch.usersList = usersList;
                    	overwatch.usersProfiles = usersProfiles;
                        mainWindow.webContents.send('users-list-exist',"true");
                        return true;
                    }
                });
            }else{
                mainWindow.webContents.send('users-list-exist',"false");
                return false;
            }
        });
    }

    function processData(data){
        var txt = data.join("\n");
        var writing = fs.writeFile(userListPath, txt,"utf8", function (err) {
            if(err){
                return "nop :"+err.message;
            }
            return "ok";
        });
        return writing;
    }

    function renderProfiles(userList){
        for (var i = 0; i < userList.length; i++) {
            getUsersInfo(userList[i]);
        }
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.